#!/usr/bin/perl
=head1 NAME

hasher.pl - file hasher (integrity checker) using SHA1 digest

=head1 VERSION

2013.0707

=head1 COPYRIGHT

Copyright: 2011-2013 Dmitry Smirnov <onlyjob@member.fsf.org>

=head1 LICENSE

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

=head1 DEPENDS

libdigest-sha-perl a.k.a. Digest::SHA

=cut

use 5.10.1;
use strict;
use warnings;
# no warnings qw(uninitialized);
#use Data::Dumper;
use Digest::SHA qw(sha1_hex sha256_hex);
use Digest::MD5;
use File::Find qw(finddepth find);
use Term::ANSIColor qw(color colored colorstrip);
    local $Term::ANSIColor::AUTORESET = 1;  #local $Term::ANSIColor::EACHLINE = "\n";
use Getopt::Long;
use autouse 'Pod::Usage'=>qw(pod2usage);
$|=1;
my %ARGV=(
    'force-digest'       =>'sha1',
    'check-only'         =>0,
    'dirty'              =>0,
    'keep-missing'       =>0,
    'squash-hard-links'  =>0,
    'exclude-vcs'        =>0,

    '_last_status'  =>0,        # last status update time()
    '_avg_speed'    =>0,
    '_avg_n'        =>0,        # moving average iteration
    '_speed'        =>1,        # processing speed bytes/sec.
    '_psize'        =>0,        # total processed size
    '_tsize'        =>0,        # total size to process
    '_ppct'         =>0,        # processed (percent)
    '_runtm'        =>0,        # total hash time (sec.)
    '_secleft'      =>0,        # estimated time to finish (sec.)
    '_progress'     =>0,        # colorised status line with stats.
    '_progress_len' =>0,        # size of status line
);
GetOptions(\%ARGV,(
        'help|?',
        'man',
        'check-only|checkonly|dry-run',
        'commit-changes',
        'squash-hard-links',
        'exclude=s@',
        'exclude-vcs',
        'force-digest=s',
        'keep-missing|keep',
        'list|check=s',
        'new-only|quick',
        'update',
        'quiet',
        'verbose|v',
)) or pod2usage(-verbose=>0);
pod2usage(-verbose=>1,-noperldoc=>1) if $ARGV{'help'};
pod2usage(-verbose=>2,-noperldoc=>0) if $ARGV{'man'};
pod2usage(-verbose=>0,-noperldoc=>1) unless $ARGV{'list'} or @ARGV;
$ARGV{'force-digest'}=lc $ARGV{'force-digest'};
pod2usage(-verbose=>0,-noperldoc=>1,-message=>"E: incorrect force-digest")
    unless $ARGV{'force-digest'} eq 'sha1'
        or $ARGV{'force-digest'} eq 'sha256';

$ARGV{'update'}=1 if defined $ARGV{'commit-changes'};
$ARGV{'keep-missing'}=1 if defined $ARGV{'new-only'};
$ARGV{'dry-run'}=1 if $ARGV{'check-only'};

my $TERMcols=qx{stty -a}=~m{columns (\d+);} ? $1 : 80;      # ($rows,$cols) = split(q{ },qx{/bin/stty size});
my $TERMcolor=$ENV{'TERM'}=~m{color} ? 1 : '';
my %status_colors=(
    'CHANGED'   =>['bright_red on_black'],
    'OK'        =>['bright_green on_black'],
    'RENAMED'   =>['bright_green on_black'],
    'NEW'       =>['bright_blue on_black'],
    'FOUND'     =>['bright_blue on_black'],
    'HL'        =>['bright_blue on_black'],    # hard link
    'MISSING'   =>['bright_yellow on_black'],
);

my %inodes;
my %stats;
my (%files,%files_new,%files_missing,%files_changed);
my (%hashes,%hashes_new,%hashes_missing);
my %tied;

END{
   for (\%files,\%files_new,\%files_missing){
        if(defined $tied{$_}){
            untie %{$_};
            unlink $tied{$_} or warn "W: Could not unlink $tied{$_}: $!";
        }
    }
}
$SIG{INT}=$SIG{TERM}=sub{
                        print STDERR "\nInterrupted: ";
                        print STDERR "$stats{$_} $_; " for sort keys %stats;
                        print STDERR "\n";
                        exit 2;
                     };

my @sp=('←','↖','↑','↗','→','↘','↓','↙');
my $spi=1; my $spn=1;
my $scantime=time();

if(open my $FH, '<', $ARGV{'list'}){
    print STDERR "Loading digests" unless defined $ARGV{'commit-changes'};
    while(<$FH>){
        if(defined $ARGV{'update'}
           and m{^#\[CHANGED\]:([0-9a-z]{40,64})\s+([^\n\r]+)}sm
        ){
            $files_changed{$2}=$1;
        }elsif(m{^([0-9a-z]{32,64})\s+([^\n\r]+)}sm){
            next if excluded($2);
            if(-l $2 or -e -r _){  # exist, readable OR symlink
                $files{$2}=$1;
                $hashes{$1}='';
                $ARGV{'_tsize'}+= -s $2 if not -l $2 and not defined $ARGV{'new-only'};
                exe_assray(\%files);
            }else{                 # missing
                $files_missing{$2}=$1;
                $hashes_missing{$1}='';
                exe_assray(\%files_missing);
            }
        }
        print STDERR $sp[$spi++%$#sp],"\b" unless ++$spn%1000;  # spinner
    }
    close $FH;
    print STDERR ". ";
    if(defined $ARGV{'update'} and keys %files_changed){
        @files{keys %files_changed}=values %files_changed;
        delete @files_changed{keys %files_changed};
        $ARGV{'dirty'}=1;
        if(defined $ARGV{'commit-changes'}){
            save_results($ARGV{'list'});
            exit;
        }
    }
    exit if(defined $ARGV{'commit-changes'});   # exit if no changes to commit
}

print STDERR "Scanning directories" if @ARGV;
while($_=shift @ARGV){
    s{^\./}{};        # drop prefix ./
    finddepth {
            no_chdir=>1,
            wanted=>sub{
                    return if $_ eq '.' or $_ eq '..';
                    return if -d $File::Find::name;
                    return if excluded($File::Find::name);
                    return if defined $files{$File::Find::name};   # not a new file
                    return if -p $File::Find::name or -S _ or -b _ or -c _;  # not a (named pipe, socket, block special file or character special file);

                    $ARGV{'_tsize'}+= -s $File::Find::name unless -l $File::Find::name;
                    $files_new{$File::Find::name}='';
                    print STDERR $sp[$spi++%$#sp],"\b" unless ++$spn%1000;  # spinner

                    exe_assray(\%files_new);
            }
    }, $_;
    print STDERR '.';
}

print STDERR ' '
##            ,scalar(%files) + scalar(%files_new),' files'
#            ,' ('
#            ,scalar values %files_new,' new files'
##            ,scalar %files_missing,' missing'
#            ,'); '
            ,sprintf("%.03f",$ARGV{'_tsize'}/1024/1024/1024)
            ," GiB to hash ($ARGV{'force-digest'}).\n";

my $status;
my $starttm=time();
$ARGV{'_last_status'}=$starttm+2;   # statistics delayed for 2 seconds (important for accuracy).
$scantime=$starttm-$scantime;

while (my $fil=each %files_new){
    my ($hexdigest,$newdigest,$cached)=hashme($fil);
    if($ARGV{'squash-hard-links'} and $cached){
        $status='HL';
    }
    else{
        $status='NEW';
        $files_new{$fil}=$hexdigest;
        $hashes_new{$hexdigest}='';
        if(defined $hashes_missing{$hexdigest}){
            $status='RENAMED';
        }
        elsif($cached or defined $hashes{$hexdigest}){
            $status='FOUND';
        }
    }
    status($status,$fil);
}
while (my $fil=each %files_missing){
    next if defined $hashes_new{$files_missing{$fil}};  # RENAMED, already processed in files_new loop.
    $status='MISSING';
    $status='FOUND' if defined $hashes{$files_missing{$fil}};
    status($status,$fil);
}
undef %hashes;
undef %hashes_new;
undef %hashes_missing;

unless (defined $ARGV{'new-only'}){
    while (my $fil=each %files){
        my ($hexdigest,$newdigest,$cached)=hashme($fil,$files{$fil});
        if(not $hexdigest and not $newdigest){
            $status='MISSING';
            $files_missing{$fil}=$files{$fil} if $files{$fil} ne '';
            delete $files{$fil};
        }
        elsif($ARGV{'squash-hard-links'} and $cached){
            $status='HL';
            delete $files{$fil};
        }
        elsif($hexdigest ne '' and $files{$fil} eq $hexdigest){
            $status='OK';
            $files{$fil}=$newdigest ? $newdigest
                                    : $hexdigest;
        }
        elsif($hexdigest ne ''){
            $status='CHANGED';
            $files_changed{$fil}=$files{$fil};     # saving old digest
        }
        status($status,$fil);
    }
}

#####################

print STDERR "\b" x $ARGV{'_progress_len'};
print STDERR color('white on_black') if $TERMcolor;

# time spent
$_='time spent '
   .($TERMcolor && color('bright_white on_black'))
    .time_from_seconds(time()-$starttm+$scantime)
   .($TERMcolor && color('white on_black'))
   . ' |';
$ARGV{'_progress'}=~s{time\s+left[^|]+\|}{$_};
$ARGV{'_progress'}=colorstrip($ARGV{'_progress'}) unless $TERMcolor;

print STDERR $ARGV{'_progress'},"\n";

if($ARGV{'dirty'}
    or $stats{'NEW'}
    or $stats{'RENAMED'}
    or $stats{'CHANGED'}
    or ( $stats{'FOUND'}   and $ARGV{'keep-missing'}==0 )
    or ( $stats{'MISSING'} and $ARGV{'keep-missing'}==0 )
){
    if(defined $ARGV{'list'}){
        save_results($ARGV{'list'});
    }else{
        print_results();
    }
}

exit;


=begin comment

    # return string "HH:MM:SS" from raw number of seconds
    time_from_seconds($seconds)

=end comment
=cut
sub time_from_seconds {
    my $seconds=shift;
    local $_=
             sprintf("%2d",$seconds/60/60) .':'
            .sprintf("%02d",($seconds/60)%60) .':'
            .sprintf("%02d",$seconds%60)
    ;
return $_;
}

=begin comment

    # externalise hashes if needed
    exe_assray(\%hash,$size)

    HASH-tie/DB_File if greater number of elements than $size.

=end comment
=cut
sub exe_assray {
    my ($hash_ref,$size)=@_;
    return 1 if defined $tied{$hash_ref};   # already tied
    $size||=1_000_000;
    if($size < scalar keys %{$hash_ref}){
        use File::Temp qw(tempfile);
        my (undef, $tmp_filename) = tempfile(OPEN=>0, SUFFIX=>'.hasher');
#print STDERR "\n:: Externalising $hash_ref $tmp_filename...";
        my %tmphash=%{$hash_ref};
        %{$hash_ref}=();

        use DB_File;
        #tie %{$hash_ref}, 'DB_File', undef, O_CREAT|O_RDWR, 0640, $DB_BTREE;    # in RAM
        tie %{$hash_ref}, 'DB_File', $tmp_filename, O_CREAT|O_RDWR, 0640, $DB_BTREE
            or die "Can't tie/DB_File: $!\n";

        #use GDBM_File;     # slower than DB_File
        #tie %{$hash_ref}, 'GDBM_File', $tmp_filename, &GDBM_WRCREAT, 0640
        #    or die "Can't tie/GDBM_File: $!\n";

        %{$hash_ref}=%tmphash;
        undef %tmphash;
        $tied{$hash_ref}=$tmp_filename;
#print STDERR "done.\n";
    }
}


=begin comment

    status($file_status,$file_name);

=end comment
=cut
sub status {
    my ($status,$fil)=@_;
    $stats{$status}+=1;

    # progress and speed printout
    $ARGV{'_progress'}=q{};

    $ARGV{'_ppct'}= $ARGV{'_tsize'}!=0
                  ? int($ARGV{'_psize'}/$ARGV{'_tsize'}*100)
                  : 100
    ;

    # recalculate speed once per second only.
    my $time=time();
    if($ARGV{'_last_status'} < $time){
        $ARGV{'_last_status'}=$time;

        $ARGV{'_runtm'}=($time-$starttm)||1;     # ||1 is workaround for division by zero on first second.
        $ARGV{'_speed'}=$ARGV{'_psize'}/$ARGV{'_runtm'};      # bytes per second
        # http://math.stackexchange.com/questions/106313/regular-average-calculated-accumulatively/106314#106314
        #$ARGV{'_avg_speed'}=($ARGV{'_avg_speed'}*($ARGV{'_runtm'}-1)+$ARGV{'_speed'})/$ARGV{'_runtm'};     # average speed
        $ARGV{'_avg_speed'}=($ARGV{'_avg_speed'}*(++$ARGV{'_avg_n'}-1)+$ARGV{'_speed'})/$ARGV{'_avg_n'};     # average speed
        $ARGV{'_secleft'}=int(($ARGV{'_tsize'}-$ARGV{'_psize'})/($ARGV{'_avg_speed'}||1));
    }

    $ARGV{'_progress'}.=
        ($TERMcolor ? colored(['bright_white on_black'],$ARGV{'_ppct'})
                     : $ARGV{'_ppct'}
         )
        .($TERMcolor && color('white on_black'))   # reset color
            .'% done ('
        .($TERMcolor && color('bright_white on_black'))
            .sprintf("%.03f",$ARGV{'_psize'}/1024/1024/1024)
        .($TERMcolor && color('white on_black'))
            .' GiB) | '
        .($TERMcolor && color('bright_white on_black'))
            .sprintf("%.02f",$ARGV{'_speed'}/1024/1024)
        .($TERMcolor && color('white on_black'))
            .' MiB/sec | '
            .'time left '
        .($TERMcolor && color('bright_white on_black'))
            . time_from_seconds($ARGV{'_secleft'})
        .($TERMcolor && color('white on_black'))
        .' |'
    ;

    for (sort keys %stats){
        $ARGV{'_progress'}.=q{ }
            .($TERMcolor ? colored(['bright_white on_black'],$stats{$_}) : $stats{$_})
            .q{ }
            .($TERMcolor && color('white on_black'))
            .($TERMcolor ? colored($status_colors{$_},$_) : $_)
            .($TERMcolor && color('white on_black'))
            .q{;}
        ;
    }

    print STDERR "\b" x $ARGV{'_progress_len'};

    if($ARGV{'verbose'}
      or ($status ne 'OK'
          and $status ne 'HL'
          and $status ne 'FOUND'
          and $status ne 'RENAMED'
         )
    ){      # print status,file_name and clean after last progress.
        unless($ARGV{'quiet'} and $status ne 'CHANGED'){
            my $pfil=$fil;
            substr $pfil,12, length($fil)-$TERMcols+18,q{  ~~~~  }
              if length($fil)+18>$TERMcols;
            print STDERR
                  ($TERMcolor ? colored('[','bright_white on_black') : '[')
                 .($TERMcolor ? colored($status_colors{$status},$status) : $status)
                 .($TERMcolor ? colored(']','bright_white on_black') : ']')
                 .($TERMcolor && color('white on_black'))   # reset color
                 .q{ }.$pfil.(q{ } x ($ARGV{'_progress_len'}-length($fil)))     # clean-up percentage if file name is shorter
                 ."\n";
        }
    }
    # print status
    $ARGV{'_progress_len'}=length(colorstrip($ARGV{'_progress'})); #my $tm_p=$ARGV{'_progress'}; $tm_p=~s{\e\[\d+(?>;\d+)*m}{}sg; $ARGV{'_progress_len'}=length($tm_p);      # http://www.perlmonks.org/?node_id=262044
    if($ARGV{'_progress_len'}>$TERMcols){
        $ARGV{'_progress'}=~s{([CFMNR])(?:HANGED|OUND|ISSING|EW|ENAMED)}{$1}g;
        $ARGV{'_progress'}=~s{time\s+}{};
        $ARGV{'_progress_len'}=length(colorstrip($ARGV{'_progress'}));
    }
    print STDERR $ARGV{'_progress'};
}


=begin comment

    ($digest,$newdigest,$cached)=hashme($file,$oldhash)

    $cached:
                    0: calculted hash
                    1: cached hash (hardlink)

=end comment
=cut
sub hashme {
    my ($fil,$oldhash)=@_;
    return (undef,undef) unless $fil;
    $oldhash||='';
    my $sha;
    my $hexdigest='';
    my $newdigest='';
    my $cached=0;
    my $filesize= -s $fil;
    $ARGV{'_psize'}+= $filesize unless -l $fil;
        #digest calculation
        my ($fid_dev,$fid_ino,$fid_nlink)=-l $fil ? ((lstat $fil)[0,1,3])
                                                  : ((stat $fil)[0,1,3]);
        return (undef,undef) unless $fid_nlink;   # file disappeared or inaccessible
        my $dev_ino=$fid_dev.'_'.$fid_ino;
        if ($dev_ino
            and defined $inodes{$dev_ino}
            and (
                    length($oldhash)==0           # new file (no old digest)
                    or
                    length($inodes{$dev_ino})==length($oldhash) # make sure digests are of the same type
                )
        ){      #load digest from inode cache
            $hexdigest=$inodes{$dev_ino};
            $cached=1;
            #$stats{'HL'}+=1;
        }else{  #calculate digest
            my $d_len=length($oldhash)||0;
            if(40==$d_len or ($d_len==0
                              and 'sha1' eq $ARGV{'force-digest'})
            ){
                $sha=Digest::SHA->new('sha1');
            }elsif(64==$d_len or ($d_len==0
                                  and 'sha256' eq $ARGV{'force-digest'})
            ){
                $sha=Digest::SHA->new('sha256');
            }elsif(32==$d_len){
                $sha=Digest::MD5->new;
            }else{
                warn "E: unknown digest: $oldhash  $fil\n";
                next;
            }
            if(-l $fil){    # symbolic link
                $sha->add(readlink $fil);
                $hexdigest=$sha->hexdigest;
            }elsif(open my $FH,'<',$fil){
                binmode $FH;
                $sha->addfile($FH);
                $hexdigest=$sha->hexdigest;
                close $FH;
            }else{
                print STDERR "\nE: can't open $fil\n";
            }
            $inodes{$dev_ino}=$hexdigest if $fid_nlink>1;   # also store in inode cache if more than 1 hard link
        }

        # FIXME: support recalculaton for symlinks
        # recalculate digest if necessary
        if( $ARGV{'force-digest'} eq 'sha1'
            and 40!=length($hexdigest)
        ){
            $sha=Digest::SHA->new('sha1');
            $sha->addfile($fil,'b');
            $newdigest=$sha->hexdigest;
        }
        elsif( $ARGV{'force-digest'} eq 'sha256'
            and 64!=length($hexdigest)
        ){
            $sha=Digest::SHA->new('sha256');
            $sha->addfile($fil,'b');
            $newdigest=$sha->hexdigest;
        }
        #elsif( $ARGV{'force-digest'} eq 'md5'
        #    and 32!=length($hexdigest)
        #){
        #    $sha=Digest::MD5->new;
        #    if(open my $FH,'<',$fil){
        #        binmode $FH;
        #        $sha->addfile($FH);
        #        $newdigest=$sha->hexdigest;
        #        close $FH;
        #    }else{
        #         print STDERR "\nE: can't open $fil\n";
        #    }
        #}
    return ($hexdigest,$newdigest,$cached);
}


=begin comment

    excluded(file)

    check if given file name match --exclude regex and return TRUE if it is.

=end comment
=cut
sub excluded {
  local $_=shift;

  # /\.kde/share/apps/kmail/dimap/\.\d+\.        # volatile: shall this be excluded by default?

  return 1 if $ARGV{'exclude-vcs'} and m{(?:^|\/?)\.(git|svn|bzr)(?:\/|\Z)};    # skip VCS folders

  for my $exclude (@{$ARGV{'exclude'}}){
    #print STDERR "EX[$exclude] $_\n" if m{$exclude};       # DEBUG PRINTING
    return 1 if m{$exclude}i;
  }
  return 0;
}

=begin comment

    save_results(file)

    Save contents of %files and %files_changed to given file.

=end comment
=cut
sub save_results {
    return 0 if defined $ARGV{'dry-run'};
    my $F=shift;
    print STDERR "Updating list file [$F]...";
    if(open my $FH, '>', $F.'~'){
        print_results($FH);
        close $FH;

        # Rename written file
        unlink $F.'~old';
        rename $F, $F.'~old';
        rename($F.'~', $F);
        #unlink $F.'~old';

        print STDERR "[OK]\n";
    }else{
        print STDERR "[Error]\n";
    }
}

=begin comment

    print_results(*FH)

    Print contents of %files and %files_changed to given file handle
    or to STDOUT.

=end comment
=cut
sub print_results {
    my $FH=$_[0]||*STDOUT;
    for (sort(keys %files_changed)){
        # print new hashes (commented)
        print {$FH} "#[CHANGED]:".$files{$_}.q{  }.$_."\n";
    }

    if($ARGV{'keep-missing'}>0){
        print {$FH} $files_missing{$_}.q{  }.$_."\n" for (keys %files_missing);
    }

    while ($_=each %files){
        if(defined $files_changed{$_}){
            ## print old digest for changed files
            print {$FH} $files_changed{$_}.q{  }.$_."\n" if $files_changed{$_} ne '';
        }else{
            print {$FH} $files{$_}.q{  }.$_."\n" if $files{$_} ne '';
        }
    }

    while (my ($file,$hash)=each %files_new){
        print {$FH} $hash.q{  }.$file."\n";
    }
}
1;
__END__

=head1 SYNOPSIS

hasher.pl [options] [file ...]

    --help               brief help message
    --man                full documentation
    --check-only         do not update list file
    --commit-changes     update new digests from previous run in list file.
    --squash-hard-links  store only one file name for all hard links
    --exclude=regex      exclude files matching regex.
    --force-digest=sha1  update SHA256 digests to SHA1
    --keep-missing       do not remove MISSING file's digests
    --list=<file>        read/save digest to/from given file
    --new-only           add new digests to list, don't verify known files.
    --update             use commented CHANGED digests from list file
    --quiet              print only progress (status), don't print files.
    --verbose            print all checked files including HL,FOUND,RENAMED.

=head1 DESCRIPTION

This program is a friendly alternative of I<sha1deep> and I<sha1sum>
with emphasis on detecting changes.

It recursively compute SHA1 of given files/directories as well as
check existing files against list of known digests.

It takes/generates output compatible with I<sha1sum> and I<sha1deep>.

Features:
    * Automatically detect SHA1/SHA256 digests.
    * Detect renaming of files.
    * Recognise hard links and avoid digest re-calculation.
    * Checking existing files and generating new digests in one go.
    * Friendly progress indication with current speed and time estimation.
    * Summary on start, end and termination of program.
    * Special handling of changed digests.
    * Skipping ".git", ".svn" and ".bzr" folders.

When CHANGED files detected their digests are printed as comments before
all other digests (to given --list file or to STDOUT).
Therefore CHANGED files can be inspected for corruption while their old
digests are preserved.
To update digests for CHANGED files one might use "--update" option
which will load updated digests from list file before checking files again.

=head1 STATUS

    The following are possible file status:
    * [CHANGED]     File contents changed.
    * [FOUND]       File was added/removed but its copy exist somewhere else.
    * [HL]          Hard link, file content is already known.
    * [MISSING]     File is not found (removed) or unreadable.
    * [NEW]         New file.
    * [OK]          Digest verified, file content not changed.
    * [RENAMED]     File was renamed or moved to another directory.

    [MISSING]--E<gt>[FOUND]: MISSING file becomes FOUND if its digest
    is found among known existing files.
    [FOUND] may be inaccurate as the corresponding file' integrity is not
    verified yet. However files marked as FOUND were deleted so we don't
    care.
    New files reported as [FOUND] if their contents (i.e. digest) is already
    known.

    [NEW]--E<gt>[RENAMED]: NEW file becomes RENAMED if its digest
    is found among MISSING files.

    [FOUND]/[RENAMED] are almost similar to [OK]: file content is the same
    but file name or location was changed.

=head1 OPTIONS

=over 8

=item B<--check-only|--checkonly|--dry-run>

    Do not update given list file at all.

=item B<--commit-changes>

    Commit commented CHANGED digests to given list file and exit.
    Useful to flush legitimate chnages without checking all files again.
    See also "--update".

=item B<--exclude=regex>

    Do not check files matching given regex. Can be given more than once.

=item B<--exclude-vcs>

    Exclude version control system directories.

=item B<--help>

    Print a brief help message and exits.

=item B<--force-digest=(sha1|sha256)>

    Update digests. This option is for conversion SHA256 to SHA1.
    By default new digests are generated using SHA1.

=item B<--keep-missing>

    Do not delete missing file's digests from list file.

=item B<--list> E<lt>fileE<gt>

    Read/write digests from/to file. New digests are always added.
    See also "--keep-missing" and "--update" options.

=item B<--man>

    Prints the manual page and exits.

=item B<--new-only>

    Add new digests to list file but don't check known files.
    Implies "--keep-missing".

=item B<--squash-hard-links>

    Store only one file name for all hard links to the same file.
    This may dramatically reduce list file size and improve processing speed.

=item B<--update>

    In this mode list file will be updated with new digests for [CHANGED]
    files. By default digests of changed files are preserved.
    For example on first pass changes are detected and reviewed: if it's
    been decided to commit new digests it should be easy enough to do without
    rehashing everything once again with "--update".

=item B<--quiet>

    Don't print processed files except for CHANGED files.

=item B<--verbose|-v>

    Print all processed files.
    By default only with status [NEW/CHANGED/RENAMED] are printed.
    Verbose mode also prints file names of all [OK] (verified) files.

=back

=head1 EXAMPLES

Run with idle IO priority to check I<dir-2-check> and update digests in
I<.hashes.txt> file:
    ionice -c3 perl hasher.pl --list=".hashes.txt" dir-2-check

Same as above but do not delete missing digests from list file:
    ionice -c3 perl hasher.pl --list=".hashes.txt" dir-2-check --keep-missing

Save digests to file, redirected from STDOUT in sha1deep style:
    hasher.pl dir-2-check another-dir-2-check >> .hashes.txt
In this example the output will be produced with relative paths
similar to "sha1deep -rl". To generate absolute paths invoke with absolute
path to directory.

=head1 CAVEATS

--force-digest
  * May break detection of renamed files;
  * Run nearly twice slower as digest has to be calculated twice:
    to check file integrity and to store new digest.

By default hasher.pl uses "--force-digest=sha1" i.e. it recognises
SHA256, SHA1, and MD5 but convert all digests to SHA1.

--exclude
  When excluding files/directories with "." it needs to be escaped twice
  because shell omit one backslash "\".
  Example":
    --exclude=/home/user/\\.      # skip all hidden files in user's home.

Unlike `sha1sum` and `sha1deep` hasher caclulates digest of symbolic links
instead of following them in order to detect symlink change even when
their destination is not available (readable).
Unlike sha1* utilities hasher do not emit run-time errors when symbolic
link destination is not readable. (Perhaps compatibility option is needed?)

=head1 AVAILABILITY

https://gitlab.com/onlyjob/hasher

=head1 TODO

Partial checks:
 * --only (opposite to --exclude) to check subtree. ???

Check archive integrity before calculating new digest. ???
(how to report errors if corrupted archive detected)

Check for terminal resize ???

sha1(sum|deep) compatibility option (symbolic links)?

=head1 CHANGELOG

2013.0707
  * Calculate digest of broken symlinks (don't follow).
  * Fixed "illegal division by zero" when there is nothing to scan.
  * Fixed hardlink detection on new files.
  * New option: --quiet

2013.0326
  * Bugfix: Keep going on vanished files.

2013.0301
  * Fixed hours in time output.
  * New option: --exclude-vcs

2013.0226
  * New option: --squash-hard-links

2013.0224 (major update)
  * Calculate symlink's digests instead of following.
  * Skip special files to avoid endless processing of chroots.
  * Fixed digest caching for hardlinked symlinks.
  * Optimised speed and memory usage, dropped sorting and HASH keys
    lookup/counting whenever possible.
  * Swap file list(s) of 1 million or more entries to temporary file(s)
    in order to process huge directory trees. Combined with memory usage
    improvements hasher now can process large file trees with millions of
    files that are impossible to process using `sha1sum` and `sha1deep`.
  * Report known files as FOUND: NEW files are only those with unique hash.
  * Time spent reported on exit to include initial tree walk and scan time.
  * Better "time left" estimation with moving average speed calculation.
  * Drop leading "./" from arguments to prevent duplication.
  * Code cleaned-up, re-structured and simplified.
  * Many bugs fixed.

2013.0106
  * fixed [FOUND] status

2013.0105
  * Fixed checking hardlinked files if known by different digests.
  * Fixed digest recalculation for hardlinks.

2013.0103
  * Trim status line if its longer than terminal width.

2013.0102
  * Don't stop if file disappeared during check.

2013.0101
  * Added options "--exclude" and "--new-only".

=cut
