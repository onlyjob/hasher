[Hasher](https://gitlab.com/onlyjob/hasher/raw/master/hasher.pl)
is a friendly alternative of "sha1deep and "sha1sum" with emphasis on
detecting changes.

It recursively compute SHA1 of given files/directories as well as check
existing files against list of known digests.

Features:

  * Automatically detect SHA1/SHA256 digests.
  * Detect renaming of files.
  * Recognise hard links and avoid digest re-calculation.
  * Checking existing files and generating new digests in one go.
  * Friendly progress indication with current speed and time estimation.
  * Summary on start, end and termination of program.
  * Special handling of changed digests.
  * Skipping ".git", ".svn" and ".bzr" folders.
